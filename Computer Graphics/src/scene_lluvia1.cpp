#include "scene_lluvia1.h"
#include <stdio.h>
#include <stdlib.h>

#include "cgmath.h"
#include "ifile.h"
#include "mat3.h"
#include "mat4.h"
#include "time.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include <vector>

#include <IL/il.h>
#include <iostream>

const int MaxParticlesl = 1000;
cgmath::vec3 CameraPositionl = cgmath::vec3(0.0f, 0.0f, 10.0f);
int num_particlesl = 1000;
float angleXl = 0.0f;
float angleZl = 0.0f;
float windl;
float zoomXl;
float zoomYl;
float zoomZl;
float speedl;
bool changel;
float accTimel;
int rateIndexl;
int rateParticlesl[] = { 10, 20, 30, 50, 75, 100 };

struct DistancesToCameral {
	int index;
	float distance;
};
DistancesToCameral distancesToCameral[MaxParticlesl];

int comparel(const void * a, const void * b)
{
	DistancesToCameral *orderA = (DistancesToCameral *)a;
	DistancesToCameral *orderB = (DistancesToCameral *)b;

	return (orderB->distance - orderA->distance);
}


scene_lluvia1::~scene_lluvia1()
{
	glDeleteProgram(shader_programl);
	glDeleteProgram(wall_programl);
	glDeleteVertexArrays(1, &vaol);
	glDeleteVertexArrays(1, &vao1l);
	glDeleteBuffers(1, &sidesVBOl);
	glDeleteBuffers(1, &indicesBufferl);
	glDeleteTextures(1, &textureIdl);
	glDeleteTextures(1, &textureID2l);
}


void scene_lluvia1::init()
{
	accTimel = 0.0f;
	changel = false;
	zoomZl = 10.0f;
	zoomYl = 0.0f;
	zoomXl = 0.0f;
	rateIndexl = 2;
	aspectl = 1.0f;

	std::vector<cgmath::vec3> sides;
	std::vector<cgmath::vec3> wallSides;
	ILuint imageID;
	ILuint imageID2;
	std::vector<unsigned int> indices = { 0, 1, 2, 0, 2, 3 };
	std::vector<unsigned int> indices1 = { 0, 1, 2, 0, 2, 3 };

	coordinatesl.push_back(cgmath::vec2(0.0f, 0.0f));
	coordinatesl.push_back(cgmath::vec2(1.0f, 0.0f));
	coordinatesl.push_back(cgmath::vec2(1.0f, 1.0f));
	coordinatesl.push_back(cgmath::vec2(0.0f, 1.0f));

	sides.push_back(cgmath::vec3(-0.4f, -0.4f, 0.5f));
	sides.push_back(cgmath::vec3(0.4f, -0.4f, 0.5f));
	sides.push_back(cgmath::vec3(0.4f, 0.4f, 0.5f));
	sides.push_back(cgmath::vec3(-0.4f, 0.4f, 0.5f));

	wallSides.push_back(cgmath::vec3(-50.0f, -50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(50.0f, -50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(50.0f, 50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(-50.0f, 50.0f, -10.5f));

	windl = 0.003f;
	speedl = 1.0f;

	for (int i = 0; i < MaxParticlesl; i++)
	{

		particlesPositionsl.push_back(cgmath::vec3(0.0f, 0.0f, 0.0f));
	}
	for (int i = 0; i < MaxParticlesl; i++)
	{
		float r3 = 0.0005f + static_cast <float> (rand()) /
			(static_cast <float> (RAND_MAX / (0.001f - 0.0005f)));
		particlesSpeedsl.push_back(cgmath::vec3(0.0f, -r3, 0.0f));
	}
	for (int i = 0; i < MaxParticlesl; i++)
	{
		distancesToCameral[i].index = i;
		distancesToCameral[i].distance = cgmath::vec3::magnitude(CameraPositionl -
			particlesPositionsl[i]);
	}
	for (int i = 0; i < MaxParticlesl; i++)
	{
		particlesLifel.push_back(0.0f);
	}

	std::vector<cgmath::vec3> normals;
	for (int i = 0; i < 4; i++)
	{
		normals.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	}



	glGenVertexArrays(1, &vaol);
	glBindVertexArray(vaol);

	glGenBuffers(1, &sidesVBOl);
	glBindBuffer(GL_ARRAY_BUFFER, sidesVBOl);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * sides.size(),
		sides.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &coordVBOl);
	glBindBuffer(GL_ARRAY_BUFFER, coordVBOl);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * coordinatesl.size(),
		coordinatesl.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &normalVBOl);
	glBindBuffer(GL_ARRAY_BUFFER, normalVBOl);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * normals.size(),
		normals.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &indicesBufferl);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBufferl);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(),
		indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	ifile shader_file;
	shader_file.read("shaders/partVert.vert");
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	glCompileShader(vertex_shader);

	shader_file.read("shaders/partFrag.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);


	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilLoadImage("images/water.png");
	glGenTextures(1, &textureIdl);
	glBindTexture(GL_TEXTURE_2D, textureIdl);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());
	ilBindImage(0);
	ilDeleteImages(1, &imageID);


	shader_programl = glCreateProgram();
	glAttachShader(shader_programl, vertex_shader);
	glAttachShader(shader_programl, fragment_shader);
	glBindAttribLocation(shader_programl, 0, "VertexSides");
	glBindAttribLocation(shader_programl, 1, "Coordinates");
	glBindAttribLocation(shader_programl, 2, "Normal");

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glLinkProgram(shader_programl);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	ilBindImage(0);
	ilDeleteImages(1, &imageID);



	glGenVertexArrays(1, &vao1l);
	glBindVertexArray(vao1l);

	glGenBuffers(1, &wallVBOl);
	glBindBuffer(GL_ARRAY_BUFFER, wallVBOl);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * wallSides.size(),
		wallSides.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &coordVBO1l);
	glBindBuffer(GL_ARRAY_BUFFER, coordVBO1l);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * coordinatesl.size(),
		coordinatesl.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &indicesWalll);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesWalll);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices1.size(),
		indices1.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	shader_file.read("shaders/wallVert.vert");
	std::string vertex_source1 = shader_file.get_contents();
	const GLchar* vertex_source_c1 = (const GLchar*)vertex_source1.c_str();
	GLuint vertex_shader1 = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader1, 1, &vertex_source_c1, nullptr);
	glCompileShader(vertex_shader1);

	shader_file.read("shaders/wallFrag.frag");
	std::string fragment_source1 = shader_file.get_contents();
	const GLchar* fragment_source_c1 = (const GLchar*)fragment_source1.c_str();
	GLuint fragment_shader1 = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader1, 1, &fragment_source_c1, nullptr);
	glCompileShader(fragment_shader1);


	ilGenImages(1, &imageID2);
	ilBindImage(imageID2);
	ilLoadImage("images/stone_wall.png");
	glGenTextures(1, &textureID2l);
	glBindTexture(GL_TEXTURE_2D, textureID2l);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());
	ilBindImage(0);
	ilDeleteImages(1, &imageID2);


	wall_programl = glCreateProgram();
	glAttachShader(wall_programl, vertex_shader1);
	glAttachShader(wall_programl, fragment_shader1);
	glBindAttribLocation(wall_programl, 0, "VertexWall");
	glBindAttribLocation(wall_programl, 1, "Coordinates");


	glLinkProgram(wall_programl);
	glDeleteShader(vertex_shader1);
	glDeleteShader(fragment_shader1);

	ilBindImage(0);
	ilDeleteImages(1, &imageID2);
}

void scene_lluvia1::update() {
	float tiempo = time::delta_time().count();
	float velDec = -9.81f * tiempo * 4.0f;

	for (int j = 0; j < MaxParticlesl; j++) {
		int i = distancesToCameral[j].index;

		if (particlesLifel[i] > 0.0f) {
			float prevVel = particlesSpeedsl[i].y;
			particlesSpeedsl[i].y += velDec;
			particlesPositionsl[i].y += speedl * ((particlesSpeedsl[i].y + prevVel) / 2.0f) * tiempo;
			particlesPositionsl[i].x -= windl;
			particlesLifel[i] -= tiempo * speedl;

		}
		distancesToCameral[j].distance = cgmath::vec3::magnitude(particlesPositionsl[i]
			- CameraPositionl);
	}
	qsort(distancesToCameral, MaxParticlesl, sizeof(DistancesToCameral), comparel);
}


void scene_lluvia1::activeDrops(int rate)
{
	int index = 0;
	for (int i = 0; i < MaxParticlesl; i++)
	{
		if (index == rate) break;
		if (particlesLifel[i] <= 0.0f)
		{
			particlesLifel[i] = 4.5f + static_cast <float> (rand()) /
				(static_cast <float> (RAND_MAX / (5.5f - 3.5f)));
			float ii = -6.5f + static_cast <float> (rand()) /
				(static_cast <float> (RAND_MAX / (6.5f + 6.5f)));
			float jj = i % 50 + 7.0f + (static_cast <float> (rand()) /
				(static_cast <float> (RAND_MAX / (8.5f - 7.0f))));
			float ll = -5.0f + static_cast <float> (rand()) /
				(static_cast <float> (RAND_MAX / (5.0 + 5.0f)));
			particlesPositionsl[i] = cgmath::vec3(ii, jj, ll);
			particlesSpeedsl[i].y = 0.0007f + static_cast <float>
				(rand()) / (static_cast <float> (RAND_MAX / (0.0013f - 0.0007f)));
			index++;
		}
	}

}

void scene_lluvia1::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(wall_programl);


	float aX = cgmath::radians(angleXl);
	float aZ = cgmath::radians(angleZl);


	cgmath::vec4 xx(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 yy(0.0f, 1.0f, 0.0f, 0.0f);
	cgmath::vec4 zz(0.0f, 0.0f, 1.0f, 0.0f);

	cgmath::vec4 rotxx(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 rotxy(0.0f, cos(aX), sin(aX), 0.0f);
	cgmath::vec4 rotxz(0.0f, -sin(aX), cos(aX), 0.0f);
	cgmath::vec4 rotxw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotx(rotxx, rotxy, rotxz, rotxw);

	cgmath::vec4 rotzx(cos(aZ), -sin(aZ), 0.0f, 0.0f);
	cgmath::vec4 rotzy(sin(aZ), cos(aZ), 0.0f, 0.0f);
	cgmath::vec4 rotzz(0.0f, 0.0f, 1.0f, 0.0f);
	cgmath::vec4 rotzw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotz(rotzx, rotzy, rotzz, rotzw);

	// View Matrix
	cgmath::mat4 view_matrix(1.0f);
	view_matrix[3][0] = zoomXl;
	view_matrix[3][1] = zoomYl;
	view_matrix[3].z = zoomZl;
	view_matrix = rotz * rotx * cgmath::mat4::inverse(view_matrix);

	// Projection Matrix
	float far = 1000.0f;
	float near = 1.0f;
	float half_fov = cgmath::radians(30.0f);

	cgmath::mat4 proj_matrix;
	proj_matrix[0][0] = 1.0f / (aspectl * tan(half_fov));
	proj_matrix[1][1] = 1.0f / tan(half_fov);
	proj_matrix[2][2] = -((far + near) / (far - near));
	proj_matrix[2][3] = -1.0f;
	proj_matrix[3][2] = -((2 * far * near) / (far - near));

	cgmath::mat4 trans1(xx, yy, zz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	GLuint v_location1 = glGetUniformLocation(wall_programl,
		"view");
	glUniformMatrix4fv(v_location1, 1, GL_FALSE, &view_matrix[0][0]);

	GLuint p_location1 = glGetUniformLocation(wall_programl,
		"projection");
	glUniformMatrix4fv(p_location1, 1, GL_FALSE, &proj_matrix[0][0]);
	GLuint trans_location1 = glGetUniformLocation(wall_programl,
		"trans");
	glUniformMatrix4fv(trans_location1, 1, GL_FALSE, &trans1[0][0]);
	GLint tex_location1 =
		glGetUniformLocation(wall_programl, "texture1");
	glUniform1i(tex_location1, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID2l);
	glBindVertexArray(vao1l);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);


	glUseProgram(shader_programl);
	accTimel += time::delta_time().count();

	if (accTimel > 0.5f)
	{
		accTimel = 0.0f;
		activeDrops(rateParticlesl[rateIndexl]);
	}
	cgmath::mat4 trans(xx, yy, zz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	GLuint v_location = glGetUniformLocation(shader_programl,
		"view");
	glUniformMatrix4fv(v_location, 1, GL_FALSE, &view_matrix[0][0]);

	GLuint p_location = glGetUniformLocation(shader_programl,
		"projection");
	glUniformMatrix4fv(p_location, 1, GL_FALSE, &proj_matrix[0][0]);

	GLint tex_location =
		glGetUniformLocation(shader_programl, "texture1");
	glUniform1i(tex_location, 0);

	GLuint lightColor = glGetUniformLocation(shader_programl,
		"LightColor");
	glUniform3f(lightColor, 1.0f, 1.0f, 1.0f);

	GLuint lightPosition = glGetUniformLocation(shader_programl,
		"LightPosition");
	glUniform3f(lightPosition, 10.0f, 0.0f, 10.0f);

	GLuint cameraPosition = glGetUniformLocation(shader_programl,
		"CameraPosition");
	glUniform3f(cameraPosition, 0.0f, 0.0f, 10.0f);

	cgmath::mat3 model(1.0f);
	GLuint mvp_model = glGetUniformLocation(shader_programl,
		"model");
	glUniformMatrix3fv(mvp_model, 1, GL_FALSE, &model[0][0]);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureIdl);
	glBindVertexArray(vaol);
	for (int j = 0; j < MaxParticlesl; j++)
	{
		int i = distancesToCameral[j].index;
		if (particlesLifel[i] > 0.0f)
		{
			trans[3][0] = particlesPositionsl[i].x;
			trans[3][1] = particlesPositionsl[i].y;
			trans[3][2] = particlesPositionsl[i].z;
			cgmath::mat3 mNormal = cgmath::mat3::transpose(cgmath::mat3::inverse(model));
			GLuint mvp_rotation = glGetUniformLocation(shader_programl,
				"mNormal");
			glUniformMatrix3fv(mvp_rotation, 1, GL_FALSE, &mNormal[0][0]);

			GLuint trans_location = glGetUniformLocation(shader_programl,
				"trans");
			glUniformMatrix4fv(trans_location, 1, GL_FALSE, &trans[0][0]);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
		}
	}
	update();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}


void scene_lluvia1::resize(int width, int height)
{
	glViewport(0, 0, width, height);

	aspectl = static_cast<float>(width) / static_cast<float>(height);
}

void scene_lluvia1::awake()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPointSize(10.0f);
}

void scene_lluvia1::sleep()
{
	glPointSize(5.0f);
}

void scene_lluvia1::normalKeysDown(unsigned char key)
{
	if (key == 'w' && rateIndexl < 5)
	{
		rateIndexl += 1;
	}
	else if (key == 's' && rateIndexl > 0) {
		rateIndexl -= 1;
	}
	else if (key == 'a' && angleXl > 0.0f)
	{
		angleXl -= 20.0f;
	}
	else if (key == 'd' && angleXl < 360.0f)
	{
		angleXl += 20.0f;
	}
	else if (key == 'z' && angleZl > 0.0f)
	{
		angleZl -= 20.0f;
		//std::cout << angleZ << std::endl;
	}
	else if (key == 'x' && angleZl < 360.0f)
	{
		angleZl += 20.0f;

	}
	else if (key == 'e' && windl > -0.015f)
	{
		windl -= 0.005f;
	}
	else if (key == 'q' && windl < 0.015f)
	{
		windl += 0.005f;
	}
	else if (key == 'i' && zoomZl > -10.0f)
	{
		zoomZl -= 2.0f;
	}
	else if (key == 'k' && zoomZl < 20.0f)
	{
		zoomZl += 2.0f;
	}
	else if (key == 'j' && zoomXl > -10.0f)
	{
		zoomXl -= 2.0f;
	}
	else if (key == 'l' && zoomXl < 10.0f)
	{
		zoomXl += 2.0f;
	}
	else if (key == 'h' && zoomYl > -28.0f)
	{
		zoomYl -= 4.0f;
		std::cout << zoomYl << std::endl;
	}
	else if (key == 'y' && zoomYl < 28.0f)
	{
		zoomYl += 4.0f;
		std::cout << zoomYl << std::endl;
	}
	else if (key == 't' && speedl < 1.8f)
	{
		speedl += 0.2f;
	}
	else if (key == 'g' && speedl > 0.3f)
	{
		speedl -= 0.2f;
	}
	else if (key == 'r')
	{
		zoomYl = 0.0f;
		zoomXl = 0.0f;
		zoomZl = 10.0f;
		angleXl = 0.0f;
		angleZl = 0.0f;
		windl = 0.005f;
		speedl = 1.0f;
	}

}