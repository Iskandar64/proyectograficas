#include "scene_fire.h"
#include <stdio.h>
#include <stdlib.h>

#include <algorithm>

#include "cgmath.h"
#include "ifile.h"
#include "mat3.h"
#include "mat4.h"
#include "time.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include <vector>
#include <IL/il.h>
#include <iostream>

const int MaxParticlesFire = 1150;
cgmath::vec3 CameraPositionFire = cgmath::vec3(0.0f, 0.0f, 10.0f);

float angleXFire = 0.0f;
float angleZFire = 0.0f;
float windFire;
float zoomXFire;
float zoomYFire;
float zoomZFire;
float speedFire;
float accTimeFire;
int rateIndexFire;
int rateParticlesFire[] = { 10, 25, 50, 75, 100, 150, 200, 300, 400};

struct DistancesToCameraFire {
	int index;
	float distance;
};
DistancesToCameraFire distancesToCameraFire[MaxParticlesFire];

float RandomFloat(float min, float max)
{
	// this  function assumes max > min, you may want 
	// more robust error checking for a non-debug build
	float random = ((float)rand()) / (float)RAND_MAX;

	// generate (in your case) a float between 0 and (4.5-.78)
	// then add .78, giving you a float between .78 and 4.5
	float range = max - min;
	return (random*range) + min;
}


bool compareFire(DistancesToCameraFire const& a, DistancesToCameraFire const& b)
{
	return (b.distance < a.distance);
}


scene_fire::~scene_fire()
{
	glDeleteProgram(shader_programFire);
	glDeleteProgram(wall_programFire);
	glDeleteVertexArrays(1, &vaoFire);
	glDeleteVertexArrays(1, &vao1Fire);
	glDeleteBuffers(1, &sidesVBOFire);
	glDeleteBuffers(1, &indicesBufferFire);
	glDeleteTextures(1, &textureIdFire);
	glDeleteTextures(1, &textureID2Fire);
}


void scene_fire::init()
{
	accTimeFire = 0.0f;
	zoomZFire = 10.0f;
	zoomYFire = 0.0f;
	zoomXFire = 0.0f;
	rateIndexFire = 3;
	aspectFire = 1.0f;

	std::vector<cgmath::vec3> sides;
	std::vector<cgmath::vec3> wallSides;
	ILuint imageID;
	ILuint imageID2;
	std::vector<unsigned int> indices = { 0, 1, 2, 0, 2, 3 };
	std::vector<unsigned int> indices1 = { 0, 1, 2, 0, 2, 3 };

	coordinatesFire.push_back(cgmath::vec2(0.0f, 0.0f));
	coordinatesFire.push_back(cgmath::vec2(1.0f, 0.0f));
	coordinatesFire.push_back(cgmath::vec2(1.0f, 1.0f));
	coordinatesFire.push_back(cgmath::vec2(0.0f, 1.0f));

	sides.push_back(cgmath::vec3(-0.1f, -0.2f, 0.5f));
	sides.push_back(cgmath::vec3(0.1f, -0.2f, 0.5f));
	sides.push_back(cgmath::vec3(0.1f, 0.2f, 0.5f));
	sides.push_back(cgmath::vec3(-0.1f, 0.2f, 0.5f));

	wallSides.push_back(cgmath::vec3(-50.0f, -50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(50.0f, -50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(50.0f, 50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(-50.0f, 50.0f, -10.5f));

	windFire = 0.003f;
	speedFire = 1.0f;

	for (int i = 0; i < MaxParticlesFire; i++)
	{

		particlesPositionsFire.push_back(cgmath::vec3(0.0f, 0.0f, 0.0f));
	}
	for (int i = 0; i < MaxParticlesFire; i++)
	{
		
		float r3 = RandomFloat(0.0005f, 0.001f);
		particlesSpeedsFire.push_back(cgmath::vec3(0.0f, -r3, 0.0f));
	}
	for (int i = 0; i < MaxParticlesFire; i++)
	{
		distancesToCameraFire[i].index = i;
		distancesToCameraFire[i].distance = cgmath::vec3::magnitude(CameraPositionFire -
			particlesPositionsFire[i]);
	}
	for (int i = 0; i < MaxParticlesFire; i++)
	{
		particlesLifeFire.push_back(0.0f);
	}

	std::vector<cgmath::vec3> normals;
	for (int i = 0; i < 4; i++)
	{
		normals.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	}



	glGenVertexArrays(1, &vaoFire);
	glBindVertexArray(vaoFire);

	glGenBuffers(1, &sidesVBOFire);
	glBindBuffer(GL_ARRAY_BUFFER, sidesVBOFire);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * sides.size(),
		sides.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &coordVBOFire);
	glBindBuffer(GL_ARRAY_BUFFER, coordVBOFire);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * coordinatesFire.size(),
		coordinatesFire.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &normalVBOFire);
	glBindBuffer(GL_ARRAY_BUFFER, normalVBOFire);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * normals.size(),
		normals.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &indicesBufferFire);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBufferFire);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(),
		indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	ifile shader_file;
	shader_file.read("shaders/partVert.vert");
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	glCompileShader(vertex_shader);

	shader_file.read("shaders/partFrag.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);


	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilLoadImage("images/ember.png");
	glGenTextures(1, &textureIdFire);
	glBindTexture(GL_TEXTURE_2D, textureIdFire);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());
	ilBindImage(0);
	ilDeleteImages(1, &imageID);


	shader_programFire = glCreateProgram();
	glAttachShader(shader_programFire, vertex_shader);
	glAttachShader(shader_programFire, fragment_shader);
	glBindAttribLocation(shader_programFire, 0, "VertexSides");
	glBindAttribLocation(shader_programFire, 1, "Coordinates");
	glBindAttribLocation(shader_programFire, 2, "Normal");

	

	glLinkProgram(shader_programFire);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	ilBindImage(0);
	ilDeleteImages(1, &imageID);



	glGenVertexArrays(1, &vao1Fire);
	glBindVertexArray(vao1Fire);

	glGenBuffers(1, &wallVBOFire);
	glBindBuffer(GL_ARRAY_BUFFER, wallVBOFire);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * wallSides.size(),
		wallSides.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &coordVBO1Fire);
	glBindBuffer(GL_ARRAY_BUFFER, coordVBO1Fire);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * coordinatesFire.size(),
		coordinatesFire.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &indicesWallFire);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesWallFire);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices1.size(),
		indices1.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	shader_file.read("shaders/wallVert.vert");
	std::string vertex_source1 = shader_file.get_contents();
	const GLchar* vertex_source_c1 = (const GLchar*)vertex_source1.c_str();
	GLuint vertex_shader1 = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader1, 1, &vertex_source_c1, nullptr);
	glCompileShader(vertex_shader1);

	shader_file.read("shaders/wallFrag.frag");
	std::string fragment_source1 = shader_file.get_contents();
	const GLchar* fragment_source_c1 = (const GLchar*)fragment_source1.c_str();
	GLuint fragment_shader1 = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader1, 1, &fragment_source_c1, nullptr);
	glCompileShader(fragment_shader1);


	ilGenImages(1, &imageID2);
	ilBindImage(imageID2);
	ilLoadImage("images/wall.jpg");
	glGenTextures(1, &textureID2Fire);
	glBindTexture(GL_TEXTURE_2D, textureID2Fire);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());
	ilBindImage(0);
	ilDeleteImages(1, &imageID2);


	wall_programFire = glCreateProgram();
	glAttachShader(wall_programFire, vertex_shader1);
	glAttachShader(wall_programFire, fragment_shader1);
	glBindAttribLocation(wall_programFire, 0, "VertexWall");
	glBindAttribLocation(wall_programFire, 1, "Coordinates");


	glLinkProgram(wall_programFire);
	glDeleteShader(vertex_shader1);
	glDeleteShader(fragment_shader1);

	ilBindImage(0);
	ilDeleteImages(1, &imageID2);
}


void scene_fire::update() {
	float tiempo = time::delta_time().count();
	float totalTiempo = time::elapsed_time().count();

	float velDec = -4.905f * tiempo;

	for (int j = 0; j < MaxParticlesFire; j++) {
		int i = distancesToCameraFire[j].index;

		if (particlesLifeFire[i] > 0.0f) {
			float prevVel = particlesSpeedsFire[i].y;
			particlesSpeedsFire[i].y += velDec;
			particlesPositionsFire[i].y += (speedFire * ((particlesSpeedsFire[i].y + prevVel) / 2.0f) * tiempo);
			
			if (particlesPositionsFire[i].x < 0.0f)
			{
				particlesPositionsFire[i].x += 0.006 * abs(particlesPositionsFire[i].x);
			}
			else {
				particlesPositionsFire[i].x -= 0.006 * abs(particlesPositionsFire[i].x);
			}
			particlesPositionsFire[i].x -= (windFire + sin(totalTiempo +
				((float)i / 100.0f)) * 0.005f);

			particlesLifeFire[i] -= tiempo * speedFire;
			if (particlesSpeedsFire[i].y < -0.5f)
			{
				particlesLifeFire[i] == 0.0f;
			}
		}
		distancesToCameraFire[j].distance = cgmath::vec3::magnitude(particlesPositionsFire[i]
			- CameraPositionFire);
	}

	std::sort(distancesToCameraFire, distancesToCameraFire + MaxParticlesFire, compareFire);
}


void scene_fire::activeDrops(int rate)
{
	int index = 0;
	for (int i = 0; i < MaxParticlesFire; i++)
	{
		if (index == rate) break;
		if (particlesLifeFire[i] <= 0.0f)
		{
			particlesLifeFire[i] = RandomFloat(1.0f, 2.0f);
			float ii = RandomFloat(-2.5f, 2.5f);
			float jj = RandomFloat(-4.0f, -3.5f);
			float ll = RandomFloat(-3.0f, 3.0f);
			
			particlesPositionsFire[i] = cgmath::vec3(ii, jj, ll);
			particlesSpeedsFire[i].y = RandomFloat(7.5f, 11.0f);
			if (ii > 0.0f && ii < 1.0)
			{
				ii += RandomFloat(0.1f, 0.6f);
			}
			if (ii < 0.0f && ii > -1.0)
			{
				ii -= RandomFloat(0.1f, 0.6f);
			}
			if (abs(ii) > 1.5)
			{
				particlesLifeFire[i] = RandomFloat(0.5f, 1.0f);
				particlesSpeedsFire[i].y = RandomFloat(5.5f, 8.0f);
			}
			index++;
		}
	}

}

void scene_fire::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(wall_programFire);


	float aX = cgmath::radians(angleXFire);
	float aZ = cgmath::radians(angleZFire);


	cgmath::vec4 xx(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 yy(0.0f, 1.0f, 0.0f, 0.0f);
	cgmath::vec4 zz(0.0f, 0.0f, 1.0f, 0.0f);

	cgmath::vec4 rotxx(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 rotxy(0.0f, cos(aX), sin(aX), 0.0f);
	cgmath::vec4 rotxz(0.0f, -sin(aX), cos(aX), 0.0f);
	cgmath::vec4 rotxw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotx(rotxx, rotxy, rotxz, rotxw);

	cgmath::vec4 rotzx(cos(aZ), -sin(aZ), 0.0f, 0.0f);
	cgmath::vec4 rotzy(sin(aZ), cos(aZ), 0.0f, 0.0f);
	cgmath::vec4 rotzz(0.0f, 0.0f, 1.0f, 0.0f);
	cgmath::vec4 rotzw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotz(rotzx, rotzy, rotzz, rotzw);

	// View Matrix
	cgmath::mat4 view_matrix(1.0f);
	view_matrix[3].x = zoomXFire;
	view_matrix[3].y = zoomYFire;
	view_matrix[3].z = zoomZFire;
	view_matrix = rotz * rotx * cgmath::mat4::inverse(view_matrix);

	// Projection Matrix
	float far = 1000.0f;
	float near = 1.0f;
	float half_fov = cgmath::radians(30.0f);

	cgmath::mat4 proj_matrix;
	proj_matrix[0][0] = 1.0f / (aspectFire * tan(half_fov));
	proj_matrix[1][1] = 1.0f / tan(half_fov);
	proj_matrix[2][2] = -((far + near) / (far - near));
	proj_matrix[2][3] = -1.0f;
	proj_matrix[3][2] = -((2 * far * near) / (far - near));

	cgmath::mat4 trans1(xx, yy, zz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	GLuint v_location1 = glGetUniformLocation(wall_programFire,
		"view");
	glUniformMatrix4fv(v_location1, 1, GL_FALSE, &view_matrix[0][0]);

	GLuint p_location1 = glGetUniformLocation(wall_programFire,
		"projection");
	glUniformMatrix4fv(p_location1, 1, GL_FALSE, &proj_matrix[0][0]);
	GLuint trans_location1 = glGetUniformLocation(wall_programFire,
		"trans");
	glUniformMatrix4fv(trans_location1, 1, GL_FALSE, &trans1[0][0]);
	GLint tex_location1 =
		glGetUniformLocation(wall_programFire, "texture1");
	glUniform1i(tex_location1, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID2Fire);
	glBindVertexArray(vao1Fire);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);


	glUseProgram(shader_programFire);

	float limit = 1.0f / (float)rateParticlesFire[rateIndexFire];
	accTimeFire += time::delta_time().count();
	if (accTimeFire > limit)
	{
		int times = (int)(accTimeFire / limit);
		activeDrops(times);
		accTimeFire = 0.0f;
	}

	cgmath::mat4 trans(xx, yy, zz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	GLuint v_location = glGetUniformLocation(shader_programFire,
		"view");
	glUniformMatrix4fv(v_location, 1, GL_FALSE, &view_matrix[0][0]);

	GLuint p_location = glGetUniformLocation(shader_programFire,
		"projection");
	glUniformMatrix4fv(p_location, 1, GL_FALSE, &proj_matrix[0][0]);

	GLint tex_location =
		glGetUniformLocation(shader_programFire, "texture1");
	glUniform1i(tex_location, 0);

	GLuint lightColor = glGetUniformLocation(shader_programFire,
		"LightColor");
	glUniform3f(lightColor, 1.0f, 1.0f, 1.0f);

	GLuint lightPosition = glGetUniformLocation(shader_programFire,
		"LightPosition");
	glUniform3f(lightPosition, 10.0f, 0.0f, 10.0f);

	GLuint cameraPosition = glGetUniformLocation(shader_programFire,
		"CameraPosition");
	glUniform3f(cameraPosition, 0.0f, 0.0f, 10.0f);

	cgmath::mat3 model(1.0f);
	GLuint mvp_model = glGetUniformLocation(shader_programFire,
		"model");
	glUniformMatrix3fv(mvp_model, 1, GL_FALSE, &model[0][0]);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureIdFire);
	glBindVertexArray(vaoFire);
	for (int j = 0; j < MaxParticlesFire; j++)
	{
		int i = distancesToCameraFire[j].index;
		if (particlesLifeFire[i] > 0.0f)
		{
			trans[3][0] = particlesPositionsFire[i].x;
			trans[3][1] = particlesPositionsFire[i].y;
			trans[3][2] = particlesPositionsFire[i].z;
			cgmath::mat3 mNormal = cgmath::mat3::transpose(cgmath::mat3::inverse(model));
			GLuint mvp_rotation = glGetUniformLocation(shader_programFire,
				"mNormal");
			glUniformMatrix3fv(mvp_rotation, 1, GL_FALSE, &mNormal[0][0]);

			GLuint trans_location = glGetUniformLocation(shader_programFire,
				"trans");
			glUniformMatrix4fv(trans_location, 1, GL_FALSE, &trans[0][0]);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
		}
	}
	update();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}


void scene_fire::resize(int width, int height)
{
	glViewport(0, 0, width, height);

	aspectFire = static_cast<float>(width) / static_cast<float>(height);
}

void scene_fire::awake()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glPointSize(10.0f);
}

void scene_fire::sleep()
{
	glPointSize(5.0f);
}

void scene_fire::normalKeysDown(unsigned char key)
{
	if (key == 'w' && rateIndexFire < 8)
	{
		rateIndexFire += 1;
	}
	else if (key == 's' && rateIndexFire > 0) {
		rateIndexFire -= 1;
	}
	else if (key == 'a' && angleXFire > 0.0f)
	{
		angleXFire -= 20.0f;
	}
	else if (key == 'd' && angleXFire < 360.0f)
	{
		angleXFire += 20.0f;
	}
	else if (key == 'z' && angleZFire > 0.0f)
	{
		angleZFire -= 20.0f;
		//std::cout << angleZ << std::endl;
	}
	else if (key == 'x' && angleZFire < 360.0f)
	{
		angleZFire += 20.0f;

	}
	else if (key == 'e' && windFire > -0.015f)
	{
		windFire -= 0.003f;
	}
	else if (key == 'q' && windFire < 0.015f)
	{
		windFire += 0.003f;
	}
	else if (key == 'i' && zoomZFire > -10.0f)
	{
		zoomZFire -= 2.0f;
	}
	else if (key == 'k' && zoomZFire < 20.0f)
	{
		zoomZFire += 2.0f;
	}
	else if (key == 'j' && zoomXFire > -10.0f)
	{
		zoomXFire -= 1.5f;
	}
	else if (key == 'l' && zoomXFire < 10.0f)
	{
		zoomXFire += 1.5f;
	}
	else if (key == 'h' && zoomYFire > -20.0f)
	{
		zoomYFire -= 3.0f;
		std::cout << zoomYFire << std::endl;
	}
	else if (key == 'y' && zoomYFire < 20.0f)
	{
		zoomYFire += 3.0f;
		std::cout << zoomYFire << std::endl;
	}
	else if (key == 't' && speedFire < 1.8f)
	{
		speedFire += 0.2f;
	}
	else if (key == 'g' && speedFire > 0.3f)
	{
		speedFire -= 0.2f;
	}
	else if (key == 'r')
	{
		zoomYFire = 0.0f;
		zoomXFire = 0.0f;
		zoomZFire = 10.0f;
		angleXFire = 0.0f;
		angleZFire = 0.0f;
		windFire = 0.003f;
		speedFire = 1.0f;
	}

}