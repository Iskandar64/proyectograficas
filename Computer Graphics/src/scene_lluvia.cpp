
#include "scene_lluvia.h"
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

#include "cgmath.h"
#include "ifile.h"
#include "mat3.h"
#include "mat4.h"
#include "time.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include <vector>

#include <IL/il.h>
#include <iostream>

GLuint lastUsedParticle = 0;
const int MaxParticles = 1000;
cgmath::vec3 CameraPosition = cgmath::vec3(0.0f, 0.0f, 10.0f);
float angleX = 0.0f;
float angleZ = 0.0f;
float wind;
float zoomX;
float zoomY;
float zoomZ;
float speed;
float accTime;
int rateIndex;
int rateParticles[] = {5, 10, 20, 40, 60, 100, 150, 200 };

float RandomFloatLluvia(float min, float max)
{
	float random = ((float)rand()) / (float)RAND_MAX;
	float range = max - min;
	return (random*range) + min;
}

struct DistancesToCamera {
	int index;
	float distance;
};
DistancesToCamera distancesToCamera[MaxParticles];

bool compare(DistancesToCamera const& a, DistancesToCamera const& b)
{
	return (b.distance < a.distance);
}


scene_lluvia::~scene_lluvia()
{
	glDeleteProgram(shader_program);
	glDeleteProgram(wall_program);
	glDeleteVertexArrays(1, &vao);
	glDeleteVertexArrays(1, &vao1);
	glDeleteBuffers(1, &sidesVBO);
	glDeleteBuffers(1, &indicesBuffer);
	glDeleteTextures(1, &textureId);
	glDeleteTextures(1, &textureID2);
}


void scene_lluvia::init()
{
	accTime = 0.0f;
	zoomZ = 10.0f;
	zoomY = 0.0f;
	zoomX = 0.0f;
	rateIndex = 2;
	aspect = 1.0f;

	std::vector<cgmath::vec3> sides;
	std::vector<cgmath::vec3> wallSides;
	ILuint imageID;
	ILuint imageID2;
	std::vector<unsigned int> indices = { 0, 1, 2, 0, 2, 3 };
	std::vector<unsigned int> indices1 = { 0, 1, 2, 0, 2, 3 };

	coordinates.push_back(cgmath::vec2(0.0f, 0.0f));
	coordinates.push_back(cgmath::vec2(1.0f, 0.0f));
	coordinates.push_back(cgmath::vec2(1.0f, 1.0f));
	coordinates.push_back(cgmath::vec2(0.0f, 1.0f));

	sides.push_back(cgmath::vec3(-0.4f, -0.4f, 0.5f));
	sides.push_back(cgmath::vec3(0.4f, -0.4f, 0.5f));
	sides.push_back(cgmath::vec3(0.4f, 0.4f, 0.5f));
	sides.push_back(cgmath::vec3(-0.4f, 0.4f, 0.5f));

	wallSides.push_back(cgmath::vec3(-50.0f, -50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(50.0f, -50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(50.0f, 50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(-50.0f, 50.0f, -10.5f));

	wind = 0.003f;
	speed = 1.0f;

	for (int i = 0; i < MaxParticles; i++)
	{

		particlesPositions.push_back(cgmath::vec3(0.0f, 0.0f, 0.0f));
	}
	for (int i = 0; i < MaxParticles; i++)
	{
		float r3 = RandomFloatLluvia(0.0005f, 0.001f);
		particlesSpeeds.push_back(cgmath::vec3(0.0f, -r3, 0.0f));
	}
	for (int i = 0; i < MaxParticles; i++)
	{
		distancesToCamera[i].index = i;
		distancesToCamera[i].distance = cgmath::vec3::magnitude(CameraPosition -
			particlesPositions[i]);
	}
	for (int i = 0; i < MaxParticles; i++)
	{
		particlesLife.push_back(0.0f);
	}

	std::vector<cgmath::vec3> normals;
	for (int i = 0; i < 4; i++)
	{
		normals.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	}



	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &sidesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, sidesVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * sides.size(),
		sides.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &coordVBO);
	glBindBuffer(GL_ARRAY_BUFFER, coordVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * coordinates.size(),
		coordinates.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &normalVBO);
	glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * normals.size(),
		normals.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &indicesBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(),
		indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	ifile shader_file;
	shader_file.read("shaders/partVert.vert");
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	glCompileShader(vertex_shader);

	shader_file.read("shaders/partFrag.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);


	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilLoadImage("images/water.png");
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());
	ilBindImage(0);
	ilDeleteImages(1, &imageID);


	shader_program = glCreateProgram();
	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);
	glBindAttribLocation(shader_program, 0, "VertexSides");
	glBindAttribLocation(shader_program, 1, "Coordinates");
	glBindAttribLocation(shader_program, 2, "Normal");

	

	glLinkProgram(shader_program);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	ilBindImage(0);
	ilDeleteImages(1, &imageID);



	glGenVertexArrays(1, &vao1);
	glBindVertexArray(vao1);

	glGenBuffers(1, &wallVBO);
	glBindBuffer(GL_ARRAY_BUFFER, wallVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * wallSides.size(),
		wallSides.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &coordVBO1);
	glBindBuffer(GL_ARRAY_BUFFER, coordVBO1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * coordinates.size(),
		coordinates.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &indicesWall);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesWall);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices1.size(),
		indices1.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	shader_file.read("shaders/wallVert.vert");
	std::string vertex_source1 = shader_file.get_contents();
	const GLchar* vertex_source_c1 = (const GLchar*)vertex_source1.c_str();
	GLuint vertex_shader1 = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader1, 1, &vertex_source_c1, nullptr);
	glCompileShader(vertex_shader1);

	shader_file.read("shaders/wallFrag.frag");
	std::string fragment_source1 = shader_file.get_contents();
	const GLchar* fragment_source_c1 = (const GLchar*)fragment_source1.c_str();
	GLuint fragment_shader1 = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader1, 1, &fragment_source_c1, nullptr);
	glCompileShader(fragment_shader1);


	ilGenImages(1, &imageID2);
	ilBindImage(imageID2);
	ilLoadImage("images/wall.jpg");
	glGenTextures(1, &textureID2);
	glBindTexture(GL_TEXTURE_2D, textureID2);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());
	ilBindImage(0);
	ilDeleteImages(1, &imageID2);


	wall_program = glCreateProgram();
	glAttachShader(wall_program, vertex_shader1);
	glAttachShader(wall_program, fragment_shader1);
	glBindAttribLocation(wall_program, 0, "VertexWall");
	glBindAttribLocation(wall_program, 1, "Coordinates");


	glLinkProgram(wall_program);
	glDeleteShader(vertex_shader1);
	glDeleteShader(fragment_shader1);

	ilBindImage(0);
	ilDeleteImages(1, &imageID2);
}


void scene_lluvia::update() {
	float tiempo = time::delta_time().count();
	float velDec = -9.81f * tiempo * 4.0f;

	for (int j = 0; j < MaxParticles; j++) {
		int i = distancesToCamera[j].index;

		if (particlesLife[i] > 0.0f) {
			float prevVel = particlesSpeeds[i].y;
			particlesSpeeds[i].y += velDec;
			particlesPositions[i].y += speed * ((particlesSpeeds[i].y + prevVel) / 2.0f) * tiempo;
			particlesPositions[i].x -= wind;
			particlesLife[i] -= tiempo * speed;

		}
		distancesToCamera[j].distance = cgmath::vec3::magnitude(particlesPositions[i]
			- CameraPosition);
	}
	std::sort(distancesToCamera, distancesToCamera + MaxParticles,  compare);
}


void scene_lluvia::activeDrops(int rate)
{
	int index = 0;
	for (int i = 0; i < MaxParticles; i++)
	{
		if (index == rate) break;
		if (particlesLife[i] <= 0.0f)
		{
			particlesLife[i] = RandomFloatLluvia(4.0, 5.5);
			float ii = RandomFloatLluvia(-7.5f, 7.5f);
			float jj =  RandomFloatLluvia(7.5f, 9.5f); 
			float ll = RandomFloatLluvia(-5.0f, 5.0f);
			particlesPositions[i] = cgmath::vec3(ii, jj, ll);
			particlesSpeeds[i].y = RandomFloatLluvia(0.0007, 0.0013);
			index++;
		}
	}
}


void scene_lluvia::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(wall_program);


	float aX = cgmath::radians(angleX);
	float aZ = cgmath::radians(angleZ);


	cgmath::vec4 xx(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 yy(0.0f, 1.0f, 0.0f, 0.0f);
	cgmath::vec4 zz(0.0f, 0.0f, 1.0f, 0.0f);

	cgmath::vec4 rotxx(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 rotxy(0.0f, cos(aX), sin(aX), 0.0f);
	cgmath::vec4 rotxz(0.0f, -sin(aX), cos(aX), 0.0f);
	cgmath::vec4 rotxw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotx(rotxx, rotxy, rotxz, rotxw);

	cgmath::vec4 rotzx(cos(aZ), -sin(aZ), 0.0f, 0.0f);
	cgmath::vec4 rotzy(sin(aZ), cos(aZ), 0.0f, 0.0f);
	cgmath::vec4 rotzz(0.0f, 0.0f, 1.0f, 0.0f);
	cgmath::vec4 rotzw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotz(rotzx, rotzy, rotzz, rotzw);

	// View Matrix
	cgmath::mat4 view_matrix(1.0f);
	view_matrix[3].x = zoomX;
	view_matrix[3].y= zoomY;
	view_matrix[3].z = zoomZ;
	view_matrix = rotz * rotx * cgmath::mat4::inverse(view_matrix);

	// Projection Matrix
	float far = 1000.0f;
	float near = 1.0f;
	float half_fov = cgmath::radians(30.0f);

	cgmath::mat4 proj_matrix;
	proj_matrix[0][0] = 1.0f / (aspect * tan(half_fov));
	proj_matrix[1][1] = 1.0f / tan(half_fov);
	proj_matrix[2][2] = -((far + near) / (far - near));
	proj_matrix[2][3] = -1.0f;
	proj_matrix[3][2] = -((2 * far * near) / (far - near));

	cgmath::mat4 trans1(xx, yy, zz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	GLuint v_location1 = glGetUniformLocation(wall_program,
		"view");
	glUniformMatrix4fv(v_location1, 1, GL_FALSE, &view_matrix[0][0]);

	GLuint p_location1 = glGetUniformLocation(wall_program,
		"projection");
	glUniformMatrix4fv(p_location1, 1, GL_FALSE, &proj_matrix[0][0]);
	GLuint trans_location1 = glGetUniformLocation(wall_program,
		"trans");
	glUniformMatrix4fv(trans_location1, 1, GL_FALSE, &trans1[0][0]);
	GLint tex_location1 =
		glGetUniformLocation(wall_program, "texture1");
	glUniform1i(tex_location1, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID2);
	glBindVertexArray(vao1);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);


	glUseProgram(shader_program);

	float limit = 1.0f / (float)rateParticles[rateIndex];
	accTime += time::delta_time().count();
	if (accTime > limit)
	{
		int times = (int) (accTime / limit);
		activeDrops(times);
		accTime = 0.0f;
	}


	cgmath::mat4 trans(xx, yy, zz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	GLuint v_location = glGetUniformLocation(shader_program,
		"view");
	glUniformMatrix4fv(v_location, 1, GL_FALSE, &view_matrix[0][0]);

	GLuint p_location = glGetUniformLocation(shader_program,
		"projection");
	glUniformMatrix4fv(p_location, 1, GL_FALSE, &proj_matrix[0][0]);

	GLint tex_location =
		glGetUniformLocation(shader_program, "texture1");
	glUniform1i(tex_location, 0);

	GLuint lightColor = glGetUniformLocation(shader_program,
		"LightColor");
	glUniform3f(lightColor, 1.0f, 1.0f, 1.0f);

	GLuint lightPosition = glGetUniformLocation(shader_program,
		"LightPosition");
	glUniform3f(lightPosition, 10.0f, 0.0f, 10.0f);

	GLuint cameraPosition = glGetUniformLocation(shader_program,
		"CameraPosition");
	glUniform3f(cameraPosition, 0.0f, 0.0f, 10.0f);

	cgmath::mat3 model(1.0f);
	GLuint mvp_model = glGetUniformLocation(shader_program,
		"model");
	glUniformMatrix3fv(mvp_model, 1, GL_FALSE, &model[0][0]);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glBindVertexArray(vao);
	for (int j = 0; j < MaxParticles; j++)
	{
		int i = distancesToCamera[j].index;
		if (particlesLife[i] > 0.0f)
		{
			trans[3][0] = particlesPositions[i].x;
			trans[3][1] = particlesPositions[i].y;
			trans[3][2] = particlesPositions[i].z;
			cgmath::mat3 mNormal = cgmath::mat3::transpose(cgmath::mat3::inverse(model));
			GLuint mvp_rotation = glGetUniformLocation(shader_program,
				"mNormal");
			glUniformMatrix3fv(mvp_rotation, 1, GL_FALSE, &mNormal[0][0]);

			GLuint trans_location = glGetUniformLocation(shader_program,
				"trans");
			glUniformMatrix4fv(trans_location, 1, GL_FALSE, &trans[0][0]);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
		}
	}
	update();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}


void scene_lluvia::resize(int width, int height)
{
	glViewport(0, 0, width, height);

	aspect = static_cast<float>(width) / static_cast<float>(height);
}

void scene_lluvia::awake()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPointSize(10.0f);
}

void scene_lluvia::sleep()
{
	glPointSize(5.0f);
}

void scene_lluvia::normalKeysDown(unsigned char key)
{
	if (key == 'w' && rateIndex < 7)
	{
		rateIndex+= 1;
	}
	else if (key == 's' && rateIndex > 0) {
		rateIndex -= 1;
	}
	else if (key == 'a' && angleX > 0.0f)
	{
		angleX -= 20.0f;
	}
	else if (key == 'd' && angleX < 360.0f)
	{
		angleX += 20.0f;
	}
	else if (key == 'z' && angleZ > 0.0f)
	{
		angleZ -= 20.0f;
		//std::cout << angleZ << std::endl;
	}
	else if (key == 'x' && angleZ < 360.0f)
	{
		angleZ += 20.0f;

	}
	else if (key == 'e' && wind > -0.025f)
	{
		wind -= 0.005f;
	}
	else if (key == 'q' && wind < 0.025f)
	{
		wind += 0.005f;
	}
	else if (key == 'i' && zoomZ > -10.0f)
	{
		zoomZ -= 2.0f;
	}
	else if (key == 'k' && zoomZ < 20.0f)
	{
		zoomZ += 2.0f;
	}
	else if (key == 'j' && zoomX > -10.0f)
	{
		zoomX -= 1.5f;
	}
	else if (key == 'l' && zoomX < 10.0f)
	{
		zoomX += 1.5f;
	}
	else if (key == 'h' && zoomY > -20.0f)
	{
		zoomY -= 3.0f;
		std::cout << zoomY << std::endl;
	}
	else if (key == 'y' && zoomY < 20.0f)
	{
		zoomY += 3.0f;
		std::cout << zoomY << std::endl;
	}
	else if (key == 't' && speed < 1.8f)
	{
		speed += 0.2f;
	}
	else if (key == 'g' && speed > 0.3f)
	{
		speed -= 0.2f;
	}
	else if (key == 'r')
	{
		zoomY = 0.0f;
		zoomX = 0.0f;
		zoomZ = 10.0f;
		angleX = 0.0f;
		angleZ = 0.0f;
		wind = 0.005f;
		speed = 1.0f;
	}

}