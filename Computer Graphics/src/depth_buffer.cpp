#include <stdio.h>
#include <stdlib.h>
#include "cgmath.h"
#include "ifile.h"
#include <vector>
#include <IL/il.h>
#include <iostream>
#include <depth_buffer.h>

 depth_buffer::~depth_buffer()
 {

 }

 void depth_buffer::create(int resolution)
 {
	 glGenFramebuffers(1, &_depthmap);
	 const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

	 glGenTextures(1, &_framebuffer);
	 glBindTexture(GL_TEXTURE_2D, _framebuffer);
	 glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		 SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


	 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
		 GL_CLAMP_TO_BORDER, 0);
	 glDrawBuffer(GL_NONE);
	 glReadBuffer(GL_NONE);
 }

 void depth_buffer::bind()
 {

	 glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
 }

 void depth_buffer::unbind()
 {
	 glBindFramebuffer(GL_FRAMEBUFFER, 0);
 }

 void depth_buffer::bindDepthMap()
 {
 }

 void depth_buffer::unbindDepthMap()
 {
 }

 depth_buffer::depth_buffer()
 {
 }
