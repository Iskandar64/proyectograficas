#include "scene_snow.h"
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

#include "cgmath.h"
#include "ifile.h"
#include "mat3.h"
#include "mat4.h"
#include "time.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include <vector>
#include <IL/il.h>
#include <iostream>

const int MaxParticlesSnow = 1000;
cgmath::vec3 CameraPositionSnow = cgmath::vec3(0.0f, 0.0f, 10.0f);

float angleXSnow = 0.0f;
float angleZSnow = 0.0f;
float windSnow;
float zoomXSnow;
float zoomYSnow;
float zoomZSnow;
float speedSnow;
float accTimeSnow;
int rateIndexSnow;
int rateParticlesSnow[] = { 5, 10, 20, 30, 50, 75, 100, 150, 200 };

struct DistancesToCameraSnow {
	int index;
	float distance;
};
DistancesToCameraSnow distancesToCameraSnow[MaxParticlesSnow];

float RandomFloatSnow(float min, float max)
{
	float random = ((float)rand()) / (float)RAND_MAX;
	float range = max - min;
	return (random*range) + min;
}

bool compareSnow( DistancesToCameraSnow const& a,  DistancesToCameraSnow const& b)
{
	return (b.distance < a.distance);
}

scene_snow::~scene_snow()
{
	glDeleteProgram(shader_programSnow);
	glDeleteProgram(wall_programSnow);
	glDeleteVertexArrays(1, &vaoSnow);
	glDeleteVertexArrays(1, &vao1Snow);
	glDeleteBuffers(1, &sidesVBOSnow);
	glDeleteBuffers(1, &indicesBufferSnow);
	glDeleteTextures(1, &textureIdSnow);
	glDeleteTextures(1, &textureID2Snow);
}


void scene_snow::init()
{
	accTimeSnow = 0.0f;
	zoomZSnow = 10.0f;
	zoomYSnow = 0.0f;
	zoomXSnow = 0.0f;
	rateIndexSnow = 1;
	aspectSnow = 1.0f;

	std::vector<cgmath::vec3> sides;
	std::vector<cgmath::vec3> wallSides;
	ILuint imageID;
	ILuint imageID2;
	std::vector<unsigned int> indices = { 0, 1, 2, 0, 2, 3 };
	std::vector<unsigned int> indices1 = { 0, 1, 2, 0, 2, 3 };

	coordinatesSnow.push_back(cgmath::vec2(0.0f, 0.0f));
	coordinatesSnow.push_back(cgmath::vec2(1.0f, 0.0f));
	coordinatesSnow.push_back(cgmath::vec2(1.0f, 1.0f));
	coordinatesSnow.push_back(cgmath::vec2(0.0f, 1.0f));

	sides.push_back(cgmath::vec3(-0.4f, -0.4f, 0.5f));
	sides.push_back(cgmath::vec3(0.4f, -0.4f, 0.5f));
	sides.push_back(cgmath::vec3(0.4f, 0.4f, 0.5f));
	sides.push_back(cgmath::vec3(-0.4f, 0.4f, 0.5f));

	wallSides.push_back(cgmath::vec3(-50.0f, -50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(50.0f, -50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(50.0f, 50.0f, -10.5f));
	wallSides.push_back(cgmath::vec3(-50.0f, 50.0f, -10.5f));

	windSnow = 0.003f;
	speedSnow = 1.0f;

	for (int i = 0; i < MaxParticlesSnow; i++)
	{

		particlesPositionsSnow.push_back(cgmath::vec3(0.0f, 0.0f, 0.0f));
	}
	for (int i = 0; i < MaxParticlesSnow; i++)
	{
		float r3 = RandomFloatSnow(0.0005f, 0.001f);
		particlesSpeedsSnow.push_back(cgmath::vec3(0.0f, -r3, 0.0f));
	}
	for (int i = 0; i < MaxParticlesSnow; i++)
	{
		distancesToCameraSnow[i].index = i;
		distancesToCameraSnow[i].distance = cgmath::vec3::magnitude(CameraPositionSnow -
			particlesPositionsSnow[i]);
	}
	for (int i = 0; i < MaxParticlesSnow; i++)
	{
		particlesLifeSnow.push_back(0.0f);
	}

	std::vector<cgmath::vec3> normals;
	for (int i = 0; i < 4; i++)
	{
		normals.push_back(cgmath::vec3::vec3(0.0f, 0.0f, 1.0f));
	}



	glGenVertexArrays(1, &vaoSnow);
	glBindVertexArray(vaoSnow);

	glGenBuffers(1, &sidesVBOSnow);
	glBindBuffer(GL_ARRAY_BUFFER, sidesVBOSnow);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * sides.size(),
		sides.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &coordVBOSnow);
	glBindBuffer(GL_ARRAY_BUFFER, coordVBOSnow);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * coordinatesSnow.size(),
		coordinatesSnow.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &normalVBOSnow);
	glBindBuffer(GL_ARRAY_BUFFER, normalVBOSnow);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * normals.size(),
		normals.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &indicesBufferSnow);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBufferSnow);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(),
		indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	ifile shader_file;
	shader_file.read("shaders/partVert.vert");
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	glCompileShader(vertex_shader);

	shader_file.read("shaders/partFrag.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);


	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilLoadImage("images/frozen.png");
	glGenTextures(1, &textureIdSnow);
	glBindTexture(GL_TEXTURE_2D, textureIdSnow);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());
	ilBindImage(0);
	ilDeleteImages(1, &imageID);


	shader_programSnow = glCreateProgram();
	glAttachShader(shader_programSnow, vertex_shader);
	glAttachShader(shader_programSnow, fragment_shader);
	glBindAttribLocation(shader_programSnow, 0, "VertexSides");
	glBindAttribLocation(shader_programSnow, 1, "Coordinates");
	glBindAttribLocation(shader_programSnow, 2, "Normal");

	

	glLinkProgram(shader_programSnow);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	ilBindImage(0);
	ilDeleteImages(1, &imageID);



	glGenVertexArrays(1, &vao1Snow);
	glBindVertexArray(vao1Snow);

	glGenBuffers(1, &wallVBOSnow);
	glBindBuffer(GL_ARRAY_BUFFER, wallVBOSnow);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * wallSides.size(),
		wallSides.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &coordVBO1Snow);
	glBindBuffer(GL_ARRAY_BUFFER, coordVBO1Snow);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * coordinatesSnow.size(),
		coordinatesSnow.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &indicesWallSnow);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesWallSnow);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices1.size(),
		indices1.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	shader_file.read("shaders/wallVert.vert");
	std::string vertex_source1 = shader_file.get_contents();
	const GLchar* vertex_source_c1 = (const GLchar*)vertex_source1.c_str();
	GLuint vertex_shader1 = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader1, 1, &vertex_source_c1, nullptr);
	glCompileShader(vertex_shader1);

	shader_file.read("shaders/wallFrag.frag");
	std::string fragment_source1 = shader_file.get_contents();
	const GLchar* fragment_source_c1 = (const GLchar*)fragment_source1.c_str();
	GLuint fragment_shader1 = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader1, 1, &fragment_source_c1, nullptr);
	glCompileShader(fragment_shader1);


	ilGenImages(1, &imageID2);
	ilBindImage(imageID2);
	ilLoadImage("images/wall.jpg");
	glGenTextures(1, &textureID2Snow);
	glBindTexture(GL_TEXTURE_2D, textureID2Snow);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());
	ilBindImage(0);
	ilDeleteImages(1, &imageID2);


	wall_programSnow = glCreateProgram();
	glAttachShader(wall_programSnow, vertex_shader1);
	glAttachShader(wall_programSnow, fragment_shader1);
	glBindAttribLocation(wall_programSnow, 0, "VertexWall");
	glBindAttribLocation(wall_programSnow, 1, "Coordinates");


	glLinkProgram(wall_programSnow);
	glDeleteShader(vertex_shader1);
	glDeleteShader(fragment_shader1);

	ilBindImage(0);
	ilDeleteImages(1, &imageID2);
}

void scene_snow::update() {
	float tiempo = time::delta_time().count();
	float totalTiempo = time::elapsed_time().count();

	float velDec = -4.905f * tiempo;
	
	for (int j = 0; j < MaxParticlesSnow; j++) {
		int i = distancesToCameraSnow[j].index;

		if (particlesLifeSnow[i] > 0.0f) {
			float prevVel = particlesSpeedsSnow[i].y;
			particlesSpeedsSnow[i].y += velDec;
			particlesPositionsSnow[i].y += (speedSnow * ((particlesSpeedsSnow[i].y + prevVel) / 2.0f) * tiempo);
			particlesPositionsSnow[i].x -= (windSnow + sin(totalTiempo + 
				((float) i / 75.0f)) * 0.014f);
			particlesLifeSnow[i] -= tiempo * speedSnow;
		}
		distancesToCameraSnow[j].distance = cgmath::vec3::magnitude(particlesPositionsSnow[i]
			- CameraPositionSnow);

	}	
	std::sort(distancesToCameraSnow, distancesToCameraSnow + MaxParticlesSnow,  compareSnow);
}

void scene_snow::activeDrops(int rate)
{
	int index = 0;
	for (int i = 0; i < MaxParticlesSnow; i++)
	{
		if (index == rate) break;
		if (particlesLifeSnow[i] <= 0.0f)
		{
			particlesLifeSnow[i] = RandomFloatSnow(5.5f, 4.0f);
			float ii = RandomFloatSnow(-7.5f, 7.5f);
			float jj = RandomFloatSnow(7.5f, 9.5f);
			float ll = RandomFloatSnow(-5.0f, 5.0f);
			particlesPositionsSnow[i] = cgmath::vec3(ii, jj, ll);
			particlesSpeedsSnow[i].y = RandomFloatSnow(0.0007f, 0.0013f);
			index++;
		}
	}
}

void scene_snow::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(wall_programSnow);
	

	float aX = cgmath::radians(angleXSnow);
	float aZ = cgmath::radians(angleZSnow);


	cgmath::vec4 xx(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 yy(0.0f, 1.0f, 0.0f, 0.0f);
	cgmath::vec4 zz(0.0f, 0.0f, 1.0f, 0.0f);

	cgmath::vec4 rotxx(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 rotxy(0.0f, cos(aX), sin(aX), 0.0f);
	cgmath::vec4 rotxz(0.0f, -sin(aX), cos(aX), 0.0f);
	cgmath::vec4 rotxw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotx(rotxx, rotxy, rotxz, rotxw);

	cgmath::vec4 rotzx(cos(aZ), -sin(aZ), 0.0f, 0.0f);
	cgmath::vec4 rotzy(sin(aZ), cos(aZ), 0.0f, 0.0f);
	cgmath::vec4 rotzz(0.0f, 0.0f, 1.0f, 0.0f);
	cgmath::vec4 rotzw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotz(rotzx, rotzy, rotzz, rotzw);

	// View Matrix
	cgmath::mat4 view_matrix(1.0f);
	view_matrix[3][0] = zoomXSnow;
	view_matrix[3][1] = zoomYSnow;
	view_matrix[3].z = zoomZSnow;
	view_matrix = rotz * rotx * cgmath::mat4::inverse(view_matrix);

	// Projection Matrix
	float far = 1000.0f;
	float near = 1.0f;
	float half_fov = cgmath::radians(30.0f);

	cgmath::mat4 proj_matrix;
	proj_matrix[0][0] = 1.0f / (aspectSnow * tan(half_fov));
	proj_matrix[1][1] = 1.0f / tan(half_fov);
	proj_matrix[2][2] = -((far + near) / (far - near));
	proj_matrix[2][3] = -1.0f;
	proj_matrix[3][2] = -((2 * far * near) / (far - near));

	cgmath::mat4 trans1(xx, yy, zz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	GLuint v_location1 = glGetUniformLocation(wall_programSnow,
		"view");
	glUniformMatrix4fv(v_location1, 1, GL_FALSE, &view_matrix[0][0]);

	GLuint p_location1 = glGetUniformLocation(wall_programSnow,
		"projection");
	glUniformMatrix4fv(p_location1, 1, GL_FALSE, &proj_matrix[0][0]);
	GLuint trans_location1 = glGetUniformLocation(wall_programSnow,
		"trans");
	glUniformMatrix4fv(trans_location1, 1, GL_FALSE, &trans1[0][0]);
	GLint tex_location1 =
		glGetUniformLocation(wall_programSnow, "texture1");
	glUniform1i(tex_location1, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID2Snow);
	glBindVertexArray(vao1Snow);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);


	glUseProgram(shader_programSnow);

	float limit = 1.0f / (float)rateParticlesSnow[rateIndexSnow];
	accTimeSnow += time::delta_time().count();
	if (accTimeSnow > limit)
	{
		int times = (int)(accTimeSnow / limit);
		activeDrops(times);
		accTimeSnow = 0.0f;
	}

	cgmath::mat4 trans(xx, yy, zz, cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	GLuint v_location = glGetUniformLocation(shader_programSnow,
		"view");
	glUniformMatrix4fv(v_location, 1, GL_FALSE, &view_matrix[0][0]);

	GLuint p_location = glGetUniformLocation(shader_programSnow,
		"projection");
	glUniformMatrix4fv(p_location, 1, GL_FALSE, &proj_matrix[0][0]);

	GLint tex_location =
		glGetUniformLocation(shader_programSnow, "texture1");
	glUniform1i(tex_location, 0);

	GLuint lightColor = glGetUniformLocation(shader_programSnow,
		"LightColor");
	glUniform3f(lightColor, 1.0f, 1.0f, 1.0f);

	GLuint lightPosition = glGetUniformLocation(shader_programSnow,
		"LightPosition");
	glUniform3f(lightPosition, 10.0f, 0.0f, 10.0f);

	GLuint cameraPosition = glGetUniformLocation(shader_programSnow,
		"CameraPosition");
	glUniform3f(cameraPosition, 0.0f, 0.0f, 10.0f);

	cgmath::mat3 model(1.0f);
	GLuint mvp_model = glGetUniformLocation(shader_programSnow,
		"model");
	glUniformMatrix3fv(mvp_model, 1, GL_FALSE, &model[0][0]);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureIdSnow);
	glBindVertexArray(vaoSnow);
	for (int j = 0; j < MaxParticlesSnow; j++)
	{
		int i = distancesToCameraSnow[j].index;
		if (particlesLifeSnow[i] > 0.0f)
		{
			trans[3][0] = particlesPositionsSnow[i].x;
			trans[3][1] = particlesPositionsSnow[i].y;
			trans[3][2] = particlesPositionsSnow[i].z;
			cgmath::mat3 mNormal = cgmath::mat3::transpose(cgmath::mat3::inverse(model));
			GLuint mvp_rotation = glGetUniformLocation(shader_programSnow,
				"mNormal");
			glUniformMatrix3fv(mvp_rotation, 1, GL_FALSE, &mNormal[0][0]);

			GLuint trans_location = glGetUniformLocation(shader_programSnow,
				"trans");
			glUniformMatrix4fv(trans_location, 1, GL_FALSE, &trans[0][0]);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
		}
	}
	update();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}


void scene_snow::resize(int width, int height)
{
	glViewport(0, 0, width, height);

	aspectSnow = static_cast<float>(width) / static_cast<float>(height);
}

void scene_snow::awake()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPointSize(10.0f);
}

void scene_snow::sleep()
{
	glPointSize(5.0f);
}

void scene_snow::normalKeysDown(unsigned char key)
{
	if (key == 'w' && rateIndexSnow < 8)
	{
		rateIndexSnow += 1;
	}
	else if (key == 's' && rateIndexSnow > 0) {
		rateIndexSnow -= 1;
	}
	else if (key == 'a' && angleXSnow > 0.0f)
	{
		angleXSnow -= 20.0f;
	}
	else if (key == 'd' && angleXSnow < 360.0f)
	{
		angleXSnow += 20.0f;
	}
	else if (key == 'z' && angleZSnow > 0.0f)
	{
		angleZSnow -= 20.0f;
		//std::cout << angleZ << std::endl;
	}
	else if (key == 'x' && angleZSnow < 360.0f)
	{
		angleZSnow += 20.0f;

	}
	else if (key == 'e' && windSnow > -0.015f)
	{
		windSnow -= 0.005f;
	}
	else if (key == 'q' && windSnow < 0.025f)
	{
		windSnow += 0.005f;
	}
	else if (key == 'i' && zoomZSnow > -20.0f)
	{
		zoomZSnow -= 2.0f;
	}
	else if (key == 'k' && zoomZSnow < 20.0f)
	{
		zoomZSnow += 2.0f;
	}
	else if (key == 'j' && zoomXSnow > -10.0f)
	{
		zoomXSnow -= 1.5f;
	}
	else if (key == 'l' && zoomXSnow < 10.0f)
	{
		zoomXSnow += 1.5f;
	}
	else if (key == 'h' && zoomYSnow > -20.0f)
	{
		zoomYSnow -= 3.0f;
		std::cout << zoomYSnow << std::endl;
	}
	else if (key == 'y' && zoomYSnow < 20.0f)
	{
		zoomYSnow += 3.0f;
		std::cout << zoomYSnow << std::endl;
	}
	else if (key == 't' && speedSnow < 1.9f)
	{
		speedSnow += 0.2f;
	}
	else if (key == 'g' && speedSnow > 0.3f)
	{
		speedSnow -= 0.2f;
	}
	else if (key == 'r')
	{
		zoomYSnow = 0.0f;
		zoomXSnow = 0.0f;
		zoomZSnow = 10.0f;
		angleXSnow = 0.0f;
		angleZSnow = 0.0f;
		windSnow = 0.005f;
		speedSnow = 1.0f;
	}

}