#version 330

in vec3 VertexPosition;
in vec3 VertexColor;
in vec2 Coordinates;
uniform vec3 CameraPosition;
uniform vec3 LightColor;
uniform vec3 LightPosition;

out vec4 InterpolatedColor;
out vec2 coord;

uniform mat4 ModelViewProjectionMatrix;
uniform mat3 mNormal;
uniform mat3 Model;

out vec3 ambiental;
in vec3 Normal;
out vec3 phong;


void main()
{
	ambiental = LightColor * 0.10;
	vec3 norm = mNormal * Normal; //normalize(VertexPosition);
	
	InterpolatedColor = vec4(VertexColor, 1.0f);
	vec3 pixelNormal =  Model * VertexPosition; //mNormal * VertexPosition;
	vec3 vectorL = normalize(LightPosition - pixelNormal);
	vec3 difusa = max(dot( norm, vectorL), 0.0) * LightColor;

	float specularStrength = 0.5;
	vec3 viewDir = normalize(CameraPosition - pixelNormal);
	vec3 R = reflect(-vectorL, norm);
	float spec = pow(max(dot(viewDir, R), 0.0), 32.0);
	vec3 especular = specularStrength * spec * LightColor;

	phong = ambiental + especular + difusa;

	coord = Coordinates;
	gl_Position = ModelViewProjectionMatrix * vec4(VertexPosition, 1.0f);
}