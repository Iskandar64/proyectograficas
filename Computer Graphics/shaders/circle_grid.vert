#version 330

uniform float time;
uniform float pointsPerCircle;
uniform float vertexCount;

out vec4 InterpolatedColor;

vec2 getCirclePoint(float id, float segments)
{
	float x = floor(id / 2.0f);
	float y = mod(id + 1.0f, 2.0f);

	float angle = x / segments * radians(360.0f);
	float radius = 2.0f - y;
	
	float u = radius * cos(angle);
	float v = radius * sin(angle);
	
	vec2 xy = vec2(u, v);

	return xy;
}

void main()
{
  float numSegments = 20.0f;
  float circleId = floor(gl_VertexID / pointsPerCircle);
  float circleCount = floor(vertexCount / pointsPerCircle);
  
  float width = floor(sqrt(circleCount));
  
  float x = mod(circleId, width);
  float y = floor(circleId / width);
  
  float u = x / (width - 1.0f);
  float v = y / (width - 1.0f);
  
  float xOffset = cos(time + y * 0.2f) * 0.1f;
  float yOffset = cos(time + x * 0.3f) * 0.2f;
  
  float ux = u * 2.0f - 1.0f + xOffset;
  float vy = v * 2.0f - 1.0f + yOffset;
  vec2 xy = getCirclePoint(gl_VertexID, numSegments) * 0.05f + vec2(ux, vy);
  
  gl_Position = vec4(xy, 0.0f, 1.0f);
  InterpolatedColor = vec4(cos(circleId * time * 0.2f) * cos(circleId * time * 0.2f), sin(circleId * time * 0.3f) * sin(circleId * time * 0.3f), 0.0f, 1.0f);
}