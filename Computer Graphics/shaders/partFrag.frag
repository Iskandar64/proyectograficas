#version 330

out vec4 FragColor;
in vec3 CurrentColor;
uniform sampler2D texture1;
in vec2 coord;
in vec3 phong;

void main()
{
	FragColor = texture2D(texture1, coord) * vec4(phong, 1.0);
}