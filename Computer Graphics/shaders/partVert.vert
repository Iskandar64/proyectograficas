#version 330

in vec3 VertexSides;
uniform mat4 trans;
uniform mat4 view;
uniform mat4 projection;
in vec2 Coordinates;
out vec2 coord;

out vec3 ambiental;
out vec3 phong;
uniform mat3 model;
uniform mat3 mNormal;
in vec3 Normal;
uniform vec3 CameraPosition;
uniform vec3 LightColor;
uniform vec3 LightPosition;


void main()
{
	mat4 viewTrans = view * trans;
	viewTrans[0][0] = 1;
	viewTrans[1][1] = 1;
	viewTrans[2][2] = 1;

	viewTrans[0][1] = 0;
	viewTrans[0][2] = 0;
	viewTrans[1][0] = 0;
	viewTrans[1][2] = 0;
	viewTrans[2][0] = 0;
	viewTrans[2][1] = 0;
	
	ambiental = LightColor * 0.10;
	vec3 norm = mNormal * Normal;

	vec3 pixelNormal =  model * VertexSides;
	vec3 vectorL = normalize(LightPosition - pixelNormal);
	vec3 difusa = max(dot( norm, vectorL), 0.0) * LightColor;
	float specularStrength = 0.5;
	vec3 viewDir = normalize(CameraPosition - pixelNormal);
	vec3 R = reflect(-vectorL, norm);
	float spec = pow(max(dot(viewDir, R), 0.0), 128.0);
	vec3 especular = specularStrength * spec * LightColor;

	phong = ambiental + especular + difusa;

	mat4 ModelViewProjectionMatrix = projection * viewTrans;
	coord = Coordinates;
	gl_Position = ModelViewProjectionMatrix * vec4(VertexSides, 1.0f);
}