#version 330

in vec3 VertexWall;
uniform mat4 trans;
uniform mat4 view;
uniform mat4 projection;
in vec2 Coordinates;
out vec2 coord;

void main()
{
	mat4 viewTrans = view * trans;

	mat4 ModelViewProjectionMatrix = projection * viewTrans;
	coord = Coordinates;
	gl_Position = ModelViewProjectionMatrix * vec4(VertexWall, 1.0f);
}