#version 330

out vec4 FragColor;

in vec4 InterpolatedColor;
uniform sampler2D texture1;
uniform sampler2D texture2;
in vec2 coord;
in vec3 ambiental;
in vec3 phong;

void main()
{
	vec4 x = texture2D(texture1, coord) * vec4(phong, 1.0);
	vec4 y = texture2D(texture2, coord) * vec4(phong, 1.0);

	FragColor = mix(x, y, 0.5);
}