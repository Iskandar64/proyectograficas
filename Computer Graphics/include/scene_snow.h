#pragma once

#include "scene.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"

#include <vector>

class scene_snow : public scene
{
public:
	~scene_snow();

	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }
	void update();
	void activeDrops(int rate);

private:
	GLuint shader_programSnow;
	GLuint wall_programSnow;
	GLuint vaoSnow;
	GLuint vao1Snow;
	GLuint sidesVBOSnow;
	GLuint coordVBOSnow;
	GLuint coordVBO1Snow;
	GLuint normalVBOSnow;
	GLuint wallVBOSnow;
	GLuint indicesBufferSnow;
	GLuint indicesWallSnow;

	GLuint textureIdSnow;
	GLuint textureID2Snow;
	float aspectSnow;

	std::vector<cgmath::vec3> particlesPositionsSnow;
	std::vector<cgmath::vec3> particlesSpeedsSnow;
	std::vector<cgmath::vec2> coordinatesSnow;
	std::vector<GLfloat> particlesLifeSnow;

};
