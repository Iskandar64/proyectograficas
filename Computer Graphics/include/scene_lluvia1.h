#pragma once

#include "scene.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"

#include <vector>

class scene_lluvia1 : public scene
{
public:
	~scene_lluvia1();

	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }
	void update();
	void activeDrops(int rate);

private:
	GLuint shader_programl;
	GLuint wall_programl;
	GLuint vaol;
	GLuint vao1l;
	GLuint sidesVBOl;
	GLuint colorsVBOl;
	GLuint coordVBOl;
	GLuint coordVBO1l;
	GLuint normalVBOl;
	GLuint wallVBOl;
	GLuint indicesBufferl;
	GLuint indicesWalll;

	GLuint textureIdl;
	GLuint textureID2l;
	float aspectl;
	std::vector<cgmath::vec3> particlesPositionsl;
	std::vector<cgmath::vec3> particlesSpeedsl;
	std::vector<cgmath::vec2> coordinatesl;
	std::vector<GLfloat> particlesLifel;
	std::vector<GLfloat> particlesRespawnl;

};
