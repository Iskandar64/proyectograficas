#pragma once

#include "scene.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"

#include <vector>

class scene_lluvia : public scene
{
public:
	~scene_lluvia();

	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }
	void update();
	void activeDrops(int rate);

private:
	GLuint shader_program;
	GLuint wall_program;
	GLuint vao;
	GLuint vao1;
	GLuint sidesVBO;
	GLuint colorsVBO;
	GLuint coordVBO;
	GLuint coordVBO1;
	GLuint normalVBO;
	GLuint wallVBO;
	GLuint indicesBuffer;
	GLuint indicesWall;

	GLuint textureId;
	GLuint textureID2;
	float aspect;
	std::vector<cgmath::vec4> particlesColors;
	std::vector<cgmath::vec3> particlesPositions;
	std::vector<cgmath::vec3> particlesSpeeds;
	std::vector<cgmath::vec2> coordinates;
	std::vector<GLfloat> particlesLife;
	std::vector<GLfloat> particlesRespawn;

};
