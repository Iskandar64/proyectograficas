#pragma once

#include "scene.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"

#include <vector>

class scene_fire : public scene
{
public:
	~scene_fire();

	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }
	void update();
	void activeDrops(int rate);

private:
	GLuint shader_programFire;
	GLuint wall_programFire;
	GLuint vaoFire;
	GLuint vao1Fire;
	GLuint sidesVBOFire;
	GLuint coordVBOFire;
	GLuint coordVBO1Fire;
	GLuint normalVBOFire;
	GLuint wallVBOFire;
	GLuint indicesBufferFire;
	GLuint indicesWallFire;

	GLuint textureIdFire;
	GLuint textureID2Fire;
	float aspectFire;

	std::vector<cgmath::vec3> particlesPositionsFire;
	std::vector<cgmath::vec3> particlesSpeedsFire;
	std::vector<cgmath::vec2> coordinatesFire;
	std::vector<GLfloat> particlesLifeFire;

};