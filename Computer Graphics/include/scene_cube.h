#pragma once

#include "scene.h"

class scene_cube : public scene
{
public:
	~scene_cube();

	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key) {}
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

private:
	GLuint shader_program;
	GLuint vao;
	GLuint vao2;
	GLuint positionsVBO;
	GLuint colorsVBO;
	GLuint coordVBO;
	GLuint normalVBO;
	GLuint indicesBuffer;
	GLuint floorVBO;
	GLuint indfloorBuffer;
	
	GLuint textureId;
	GLuint textureID2;
	float aspect;
};